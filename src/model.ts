export interface Actor {
  name: string;
  role: string;
  photo?: string;
}

export interface Album {
  sortTitle?: string;
  aired?: string;
  collections: string[];
  genres: string[];
  summary?: string;
}

export interface Artist {
  sortTitle?: string;
  genres: string[];
  collections: string[];
  summary?: string;
  similar: string[];
}

export interface Episode {
  title: string[];
  aired?: string;
  contentRating: string;
  summary?: string;
  directors: string[];
  writers: string[];
  rating?: number;
}

export interface Movie {
  title: string;
  sortTitle?: string;
  originalTitle: string[];
  contentRating: string;
  tagline: string[];
  studio: string[];
  aired?: string;
  summary?: string;
  rating?: number;
  genres: string[];
  collections: string[];
  actors: Actor[];
  directors: string[];
  writers: string[];
}

export interface Show {
  title: string;
  sortTitle?: string;
  originalTitle: string[];
  contentRating: string;
  tagline: string[];
  studio: string[];
  aired?: string;
  summary?: string;
  rating?: number;
  genres: string[];
  collections: string[];
  actors: Actor[];
  seasonSummary: { [key: number]: string };
}

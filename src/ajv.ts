import Ajv from "ajv";
import { parse } from "uri-js";

import { schema } from "./schema";

export enum Schema {
  Show = "show",
  Movie = "movie",
  Album = "album",
  Artist = "artist",
  Episode = "episode",
  Actor = "actor"
}

export const createAjv = (): Ajv.Ajv => {
  const ajv = new Ajv();
  ajv.addFormat("iri", value => {
    const uri = parse(value);
    return uri.reference === "absolute" && Boolean(uri.scheme);
  });
  ajv.addSchema(schema.definitions);
  ajv.addSchema(schema.actor, Schema.Actor);
  ajv.addSchema(schema.show, Schema.Show);
  ajv.addSchema(schema.movie, Schema.Movie);
  ajv.addSchema(schema.album, Schema.Album);
  ajv.addSchema(schema.artist, Schema.Artist);
  ajv.addSchema(schema.episode, Schema.Episode);
  return ajv;
};

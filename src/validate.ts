import { ErrorObject } from "ajv";
import _ from "lodash";

import { Actor, Album, Artist, Episode, Movie, Show } from "./model";
import { createAjv, Schema } from "./ajv";

export class ValidationError extends Error {
  errors: ErrorObject[];

  constructor(name: string, errors: ErrorObject[]) {
    super(`Fail to validate ${name}!`);
    this.errors = errors;
  }
}

function validateSchema<T>(schema: Schema, data: any): asserts data is T {
  const ajv = createAjv();
  const validate = ajv.getSchema(schema);
  if (!validate) {
    throw new Error(`Cannot find schema (${schema}).`);
  }
  const valid = validate(data);
  if (!valid) {
    throw new ValidationError(schema, validate.errors || []);
  }
}

export function validateActor(data: any): asserts data is Actor {
  validateSchema<Actor>(Schema.Actor, data);
}

export function validateAlbum(data: any): asserts data is Album {
  validateSchema<Album>(Schema.Album, data);
}

export function validateMovie(data: any): asserts data is Movie {
  validateSchema<Movie>(Schema.Movie, data);
}

export function validateShow(data: any): asserts data is Show {
  validateSchema<Show>(Schema.Show, data);
}

export function validateArtist(data: any): asserts data is Artist {
  validateSchema<Artist>(Schema.Artist, data);
}

export function validateEpisode(data: any): asserts data is Episode {
  validateSchema<Episode>(Schema.Episode, data);
}

const toUndefined = <T>(value: T | undefined): T | undefined =>
  _.isNil(value) ? undefined : value;

export const pickActor = (data: any): Actor => {
  validateActor(data);
  const { name, role, photo } = _.pick(data, ["name", "role", "photo"]);
  return { name, role, photo: toUndefined(photo) };
};

export const pickAlbum = (data: any): Album => {
  validateAlbum(data);
  const { sortTitle, aired, collections, genres, summary } = _.pick(data, [
    "sortTitle",
    "aired",
    "collections",
    "genres",
    "summary"
  ]);
  return {
    sortTitle: toUndefined(sortTitle),
    aired: toUndefined(aired),
    collections,
    genres,
    summary: toUndefined(summary)
  };
};

export const pickArtist = (data: any): Artist => {
  validateArtist(data);
  const { sortTitle, similar, collections, genres, summary } = _.pick(data, [
    "sortTitle",
    "similar",
    "collections",
    "genres",
    "summary"
  ]);
  return {
    sortTitle: toUndefined(sortTitle),
    collections,
    similar,
    genres,
    summary: toUndefined(summary)
  };
};

export const pickEpisode = (data: any): Episode => {
  validateEpisode(data);
  const {
    title,
    aired,
    contentRating,
    directors,
    writers,
    rating,
    summary
  } = _.pick(data, [
    "title",
    "aired",
    "contentRating",
    "directors",
    "writers",
    "rating",
    "summary"
  ]);
  return {
    title,
    aired: toUndefined(aired),
    contentRating,
    directors,
    writers,
    rating: toUndefined(rating),
    summary: toUndefined(summary)
  };
};

export const pickMovie = (data: any): Movie => {
  validateMovie(data);
  const {
    title,
    sortTitle,
    originalTitle,
    contentRating,
    tagline,
    studio,
    aired,
    summary,
    rating,
    genres,
    collections,
    actors,
    directors,
    writers
  } = _.pick(data, [
    "title",
    "sortTitle",
    "originalTitle",
    "contentRating",
    "tagline",
    "studio",
    "aired",
    "summary",
    "rating",
    "genres",
    "collections",
    "actors",
    "directors",
    "writers"
  ]);
  return {
    title,
    sortTitle: toUndefined(sortTitle),
    originalTitle,
    contentRating,
    tagline,
    studio,
    aired: toUndefined(aired),
    summary: toUndefined(summary),
    rating: toUndefined(rating),
    genres,
    collections,
    actors: actors.map(pickActor),
    directors,
    writers
  };
};

export const pickShow = (data: any): Show => {
  validateShow(data);
  const {
    title,
    sortTitle,
    originalTitle,
    contentRating,
    tagline,
    studio,
    aired,
    summary,
    rating,
    genres,
    collections,
    actors,
    seasonSummary
  } = _.pick(data, [
    "title",
    "sortTitle",
    "originalTitle",
    "contentRating",
    "tagline",
    "studio",
    "aired",
    "summary",
    "rating",
    "genres",
    "collections",
    "actors",
    "seasonSummary"
  ]);
  return {
    title,
    sortTitle: toUndefined(sortTitle),
    originalTitle,
    contentRating,
    tagline,
    studio,
    aired: toUndefined(aired),
    summary: toUndefined(summary),
    rating: toUndefined(rating),
    genres,
    collections,
    actors: actors.map(pickActor),
    seasonSummary
  };
};

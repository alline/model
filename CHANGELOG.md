## [1.1.1](https://gitlab.com/alline/model/compare/v1.1.0...v1.1.1) (2020-06-29)


### Bug Fixes

* update dependencies ([2104de2](https://gitlab.com/alline/model/commit/2104de22af97570b609015bc277a65649ad743a9))

# [1.1.0](https://gitlab.com/alline/model/compare/v1.0.2...v1.1.0) (2020-05-07)


### Features

* add validation ([36520f1](https://gitlab.com/alline/model/commit/36520f19b68bde0aa421c9b1907a2e21edd0eac3))

## [1.0.2](https://gitlab.com/alline/model/compare/v1.0.1...v1.0.2) (2020-05-06)


### Bug Fixes

* GitLab CI not including artifacts ([3572cea](https://gitlab.com/alline/model/commit/3572ceab07cc397c2e7c16999d7c8b292663f2e8))

## [1.0.1](https://gitlab.com/alline/model/compare/v1.0.0...v1.0.1) (2020-05-05)


### Bug Fixes

* add types in package.json ([3c6d8af](https://gitlab.com/alline/model/commit/3c6d8afd410094fb5542692e75b5e27ebc1e2866))

# 1.0.0 (2020-05-05)


### Features

* initial release ([903ed93](https://gitlab.com/alline/model/commit/903ed93a10f7df1bb17f7cbb57fd8a52d42c3cb1))
